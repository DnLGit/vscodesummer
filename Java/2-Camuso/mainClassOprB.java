                                        /**  Video 18:  Gli Operatori Binari:  Addizione */
                            /** Nota:  syso + Ctrl+barraSpaziatrice -> System.out.println() */

public class mainClassOprB {
    public static void main(String[] args){
        System.out.println("Video 18: Gli Operatori Binari");

// Operazione Addizione:
int i1=0, i2=0, i3=0, i4=0;
double d1=0, d2=0, d3=0, d4=0;
String s1="", s2="", s3="", s4="";
char c1=' ', c2=' ', c3=' ', c4=' ', c5=' ', c6=' ';    /** nei char gli apici sono staccati da uno spazio -> carattere vuoto */

// alcuni esempi di operazione di addizione
i1=5+3;  i2=6+i1;  i3=i1+i2;

i1=3+6+8+10;

i2= (12+6) * 2 * (i3 - (i4/5));

i1 = 1000 * 1000 * 1000 * 1000;     /** Attenzione, qui il Tipo "int" non ce la fa, stampa -727379968 */
System.out.println(i1);             /** Java non me lo dice:   sarà utile il costrutto ->  try ... catch */

// L'Opertatore addizione può essere applicato anche alle stringhe ... le concatena:
s1 = "salve";
s2 = " a tutti";
s3 = s1+s2;
System.out.println("stampa s3: " + s3); /** stampa s3: salve a tutti  */

c1 = 'A'; c2 = ' ';
//c3 = c1+c2;                             /** in presenza di variabili (c1 3 c2) e dell'operatore +  viene fatta una conversione automatica ad interi!!! */
//System.out.println("stampa c3: " + c3); /**  error: incompatible types: possible lossy conversion from int to char, vedi commento sopra */

c3 = 'A' + ' ';
System.out.println("stampa ora c3: " + c3); /** stampa ora c3: a  ---- a = al codice ASCII 65 + 32 = 97 cioè 'a' */

// per sistemare utilizziamo un cast:

c3 = (char) (c1+c2);
System.out.println("stampa ora di nuovo c3: " + c3);    /** stampa ora di nuovo c3: a --- perfetto!!! */



/** 11.09.2019 - nel pc di casa mi esce questo errore: https://github.com/redhat-developer/vscode-java/wiki/"Classpath-is-incomplete"-warning */

                            /** 12.09.2019  ->  Video 19:  Gli Operatori Binari:  Sottrazione, Moltiplivazione, Divisione ...  */

// ********** alcuni esempi di operazione di sottrazione
i1=5-3;  i2=6-i1;  i3=i1-i2;

i1=3-6-8-10;

i2= (12-6) * 2 * (i3 - (i4/5));

i1 = -1000 * 1000 * 1000 * 1000;     /** Attenzione, anche qui il Tipo "int" non ce la fa, stampa 727379968 */
System.out.println(i1);             /** Java non me lo dice:   sarà utile il costrutto ->  try ... catch */

// L'Opertatore sottrazione "NON" "NON" "NON"  può essere applicato anche alle stringhe e ai Tipi Boolean:
s1 = "salve";
s2 = " a tutti";
// s4 = s1-s2;                             /** Errore:  cosa potrà mai ... S-concatenare??? */
System.out.println("stampa s4: " + s4); /** Nessuna stampa!!!!  */


c1 = 'A'; c2 = ' ';
//c3 = c1-c2;                             /** in presenza di variabili (c1 3 c2) e dell'operatore -  viene fatta una conversione automatica ad interi!!! */
//System.out.println("stampa c3: " + c3); /**  error: incompatible types: possible lossy conversion from int to char, vedi commento sopra */

c3 = 'A' - ' ';
System.out.println("stampa ora c3: " + c3); /** stampa ora c3: !  ---- ! = al codice ASCII 65 - 32 = 33 = al simbolo '!'' */

// anche per la sottrazione, per sistemare utilizziamo un cast:

c3 = (char) (c1-c2);
System.out.println("stampa ora di nuovo c3: " + c3);    /** stampa ora di nuovo c3: ! ---> perfetto!!! */


// ********* Moltiplicazione: alcuni esempi
i1=5*3;  i2=6*i1;  i3=i1*i2;

i1=3*6*8*10;

i2= (12*6) * 2 * (i3 * (i4/5));

i1 = 1000 * 1000 * 1000 * 1000;     /** Attenzione, anche qui il Tipo "int" non ce la fa, stampa 727379968 */
System.out.println(i1);             /** Java non me lo dice:   sarà utile il costrutto ->  try ... catch */

// L'Opertatore Moltiplicazione "NON" "NON" "NON"  può essere applicato anche alle stringhe e Boolean!!!!!!

// Con i caratteri è consentito ma con valori bassi ... altrimenti si va in overflow:
c1 = 'A'; c2 = ' ';
c3 = 'A' * ' ';
System.out.println("stampa ora c3: " + c3); /** stampa ora c3: ?  ---- codice ASCII 65 * 32 = 2.080 ... perchè il simbolo '?' ??? */

c4 = (char)3; c5 = ' ';
c6 = (char) (c4*c5);
System.out.println("Stampami c6: " + c6);   /** Stampami c6: `  ->  3 * 32(' ') = 96 -> simbolo `  */


// ********* Divisione: alcuni esempi:

i1 = 5/3;
i2 = 40/15;
System.out.println("Stampami i1: " + i1 + " e i2: " + i2 );     /** Stampami i1: 1 e i2: 2  --> decimali praticamente spazzati via!!!! */

double d5 = 5.0/3.0;        /** per forzare al decimale si può aggiungere uno zero dopo la virgola ... */
double d6 = 40f/15f;        /** oppure inserire la 'f' di float dopo l'intero ... */
double d7 = (double) 40/15; /** oppure anche con un "cast" */
System.out.println("Stampami d5: " + d5 + " e d6: " + d6 );     /** Stampami d5: 1.6666666666666667 e d6: 2.6666667461395264  --> Ok!!!! */
System.out.println( "Stampami d7: " + d7);                      /** Stampami d7: 2.6666666666666665 */


// Con i caratteri è consentito ma con valori bassi ... altrimenti si va in overflow:
c1 = 'A'; c2 = ' ';
c3 = 'A' / ' ';
c4='+'/'2';
System.out.println("stampa ora c3: " + c3); /** stampa ora c3:   ......... nulla??? */
c3 = ' ' / 'A';
System.out.println("stampa di nuovo c3: " + c3); /** stampa ora c3:   ......... nulla??? */
System.out.println("stampa di nuovo c4: " + c4); /** stampa ora c4:   ......... nulla??? */

c1 = (char) 2; c2 = 'D'; c4='d';
c3 = (char) (c2/c1);
c5 = (char) (c4/c1);
System.out.println("stampami ora questo cast c3: " + c3);   /** "stampami ora questo cast c3: "  -> simbolo corretto di 34  68/2 */
System.out.println("stampami ora invece questo cast c5: " + c5);   /** "stampami ora questo cast c5: 2  -> simbolo numerico corretto di 50  100/2 */



/** Espressioni Miste, ad esempio tra interi e numeri con virgola:    */


//i1 = 100 + 34.81;
System.out.println(i1);     /** error: incompatible types -> mentre: */
d1 = 100 + 34.81;
System.out.println("Stampami invece il double: " + d1);     /** Stampami invece il double: 134.81  .... ok, corretto!!*/

// Però con un "Cast" si può risolvere il problema al Tipo intero "i1" :
i1 = 100 + (int) 34.81;
System.out.println("Stampiamo sia il double: " + d1 + " e l'intero: " + i1);        /** Stampiamo sia il double: 134.81 e l'intero: 134  --> perfetto!! */


/** Altro esempio con "Stringa" e "Carattere":      */

s1 = "cia" + 'o' + '!';     /**  Somma fra una stringa e due caratteri ... */
System.out.println("La stampa risulta: " + s1);                                     /** La stampa risulta: ciao! */

s2="pi greco: " + 3.14;
System.out.println("Stampami s2: " + s2);                                           /** Stampami s2: pi greco: 3.14 */

s3 = "cia" + true;     /**  Somma fra una stringa ed un boolean ... */
System.out.println("Stampami s3: " + s3);                                           /** Stampami s3: ciatrue */


                     /** 13.09.2019  ->  Fine Video 19   */







































    }

}