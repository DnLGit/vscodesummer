
                                        /**  Video 16:  Gli Operatori I */
                            /** Nota:  syso + Ctrl+barraSpaziatrice -> System.out.println() */

public class mainClassOprU {
    public static void main(String[] args) {
    System.out.println("Hello");

    /** Operatori:   'Unari'   'Binari'   'Ternari'  'Prefissi'   'Infissi'   'Postfissi' */

    int a = 5;
    int b = 6;                 /** il segno '-' è in questo caso un operatore 'unario', cambia solo il segno all'unica espressione che segue  */
    int z = b+2;                /** il segno '+' è in questo caso un operatore 'binario' operando fra due addendi */

    // operatore prefisso: ++a
    System.out.println(++a);    /** prefisso:  prima fa la somma, poi stampa -> Risultato = 6 (5+1)*/

    // operatore postfisso: a++
    //System.out.println(a++);    /** postfisso:  prima stampa e poi fa la somma -> Risultato = 5 */
    System.out.println(a);        /** Qui in ogni caso = 6 */

    // operatore prefisso: --b
    System.out.println(--b);    /** prefisso:  prima fa la sottrazione, poi stampa -> Risultato = 5 (6-1)*/
    System.out.println(b);      /** Qui ora = 5 */

    // operatore postfisso: b--
    System.out.println(b--);    /** postfisso:  prima stampa e poi fa la somma -> Risultato = 5 */
    System.out.println(b);      /** e qui ora = 4 */

    // operatore prefisso in Virgola mobile: ++c
    double c = 7.3;
    System.out.println(--c);
    System.out.println(c++);
    System.out.println(c);
    
                            /** Video 17:  Gli Operatori II */

    // operatore prefisso su Tipi char: ++c  ( Si ... caratteri!!!)
    char c1 = 'A';                                      /** 'A' = 65 in ASCII */
    char c2 = ++c1;                                     /** Incremento il 65 di 1 = 66 -> codice ASCII di 'B' */
    System.out.println("la variabile c2 vale: " + c2);  /** e la stampa è:  la variabile c2 vale: B */

    // altro esempio "parallelo":
    int n = 'A';
    int m = -'A';                                       /** operatore unario di cambio di segno */
    System.out.println(n + " ed il negativo " + m);
    int o = ++c1;
    System.out.println("stampa c1 incrementato due volte: " + o);   //**  */

    /* Altro operatore unario:  la negazione '!' -> corrisponde al 'Not'.*/

    int eta = 17;
    boolean maggiorenne = eta>18;
    System.out.println("la dichiarazione e': " + maggiorenne);

    // Quando però ci interessa verificare la "falsità" che torna comodo l'operatore Not -> !
    // ... per esempio verificare se 'il tipo' non è maggiorenne ...:
    System.out.println("la dichiarazione e': " + !maggiorenne);

    /* Ultimo operatore unario - la negazione bitWise: '~' -> Tilde (da considerare la rappresentazione del dato in bit) */ 

    // Esempio:  il valore 17 (dell'età) in binario è 10001 negato poi con ~ diventa 01110
    int etaTilde = ~eta;
    System.out.println(etaTilde);   /** Stampa -18   perchè non il valore di 14??? */

                        /** Fine Video 17:  Gli Operatori II */

    





















    

    
                                    
                            

 

    }
}
