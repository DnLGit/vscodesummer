public class mainClass {
    public static void main(String[] args) {
        System.out.println(5*67);
        System.out.println("evviva");  /** per ripetere la stringa sopra:  syso+Ctrl+Tab */
        System.out.println(5+3+" evviva"); /** il 2° e 3° segno + in questo caso "concatena" la sequenza di caratteri */
        System.out.println(5+"evviva"+3+"ciao"+8); /** stampa tutta separata!!! */
                                                    /** Video 9: espressioni numeriche *//** Video 9: espressioni numeriche */
        System.out.println(3.14*10);                /** risultano un sacco di cifre dopo la virgola: 31.400000000000002 ... ed un 2 ... */
        System.out.println(3.14 + " pigreco");      /** qui nessun problema, anche con la virgola il segno concatena caratteri e numeri */
        System.out.println(3.14 + 6);               /** somma di un decimale con un'intero: no problem = 9.14!!!  */
        System.out.println(3.14 + 3.14);            /** somma di due decimali: idem, no problem = 6.28!!!  */
        System.out.println(5*3 + 5*3);              /** somma di due prodotti interi: no problem = 30!!!  */
        System.out.println(5*3 + 5/3);              /** somma di due un prodotto e una divisione ... eh eh = 16 virgola "qualche cosa" ...!!!  */
        System.out.println(5+4*4);                  /** prima il prodotto e poi la somma = 21 ... altrimenti usare le parentesi!!! (regole di precedenza)*/
        System.out.println(5*4/3);                  /** operazione con stessa priorità? da sx a dx: no problem = 6 ... virgola "qualcosa"  */
        System.out.println(5.0*4.0/3.0);            /** operazione con stessa priorità? da sx a dx: no problem = 6.666666666666667 ... virgola riconosciuta  */
                                                   

        
        
    }

}