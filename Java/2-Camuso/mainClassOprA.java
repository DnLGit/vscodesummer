
                                /**  Video 20:  Gli Operatori di Assegnamento e Relazionali   */
                            /** Nota:  syso + Ctrl+barraSpaziatrice -> System.out.println() */
    
public class mainClassOprA {

    public static void main(String [] args) {
        System.out.println("Video 20:  Gli Operatori di Assegnamento");

        int n = 10;              // operatore di assegnamento nella forma più semplice

       int TotaleSpese = 0;
       TotaleSpese +=1;         // Semantica:  incrementa la Variabile della espressione dopo l'uguale = ("1" in questo caso)     
       TotaleSpese +=n;         // Semantica:  incrementa la Variabile della espressione dopo l'uguale = ("n" in questo caso, cioè 10)
                        // e quindi è possibile ...:
        TotaleSpese += n + 11 - 2;
        System.out.println("Stampa TotaleSpese: " + TotaleSpese);   // Stampa TotaleSpese: 30 (0 + 1 + n (cioè 10) + 11 - 2 = 30!!)

        /** questa scrittura è possibile non solo per "incrementare" una variabile ... ma anche con gli altri operatori */

        // per esempio:

        TotaleSpese -=1000;
        System.out.println("Stampa TotaleSpese adesso: " + TotaleSpese);        // Stampa TotaleSpese adesso: -970 (30 - 1000 = -970)

        n /= 5;            // Questo invece divide per 5 la variabile "n" -> Operatore "sempre" attaccato all'uguale!!!
        System.out.println("Stampa n adesso: " + n);     // Stampa n adesso: 2

        n *= 5;
        System.out.println("Ri-Stampa n adesso: " + n);     // Ri-Stampa n adesso: 10

        // esempio con il Modulo %:
        n %= 7;
        System.out.println("n sara': " + n);         // n sara': 3 -> prima calcola il modulo su "n" (% di 10/7 = 3) 

        // altri sono:  &=  ^=   |=    <<=    >>=    >>>=    ma sono operatori "bitabit" ... non aritmetici.


        System.out.println("Video 20:  Gli Operatori Relazionali I");    // mettono in relazione/confronto due espressioni!!!

        // il più semplice:  l'uguaglianza!  Per esempio:  5==5 (che è true)  oppure 5==3 (che è false)
       
        System.out.println("Stampa 5==5 che e' " + (5==5) + " e 5==3 che e' " + (5==3));    // Stampa 5==5 che e' true e 5==3 che e' false


        // Usando delle variabili al posto delle costanti letterali ...:
        System.out.println("Quseto sara' :" + (TotaleSpese==n));        // Quseto sara' :false  ("TotalSpese" è diverso da "n")


        // altri Operatori:  > maggiore, < minore, ! diverso ...

        int eta = 22;
        boolean maggiorenne = eta >=18;
        System.out.println(maggiorenne);        // true

        int eta2 = 18;
        boolean maggiorenne2 = eta2 !=18;
        System.out.println(maggiorenne2);       // false

                            /** 16.09.2019  ->  Fine Video 20   */
        




        












        












        
    }
}
