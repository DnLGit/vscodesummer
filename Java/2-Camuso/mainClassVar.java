import java.io.InputStream;                     /** valori per usi futuri: interi, nomi, cognomi, dati con la virgola ...alfanumerici ecc */
import java.util.Scanner;                     /** descrizioni di prodotti .... messe in aree di memoria per andarle a prendere quando poi ci serviranno */

                            /**  Video 10:  le Variabili */
                            /** Nota:  syso + Ctrl+barraSpaziatrice -> System.out.println() */

public class mainClassVar {
    public static void main(String[] args) {

        double percentualeInteresse = 2.5;     /** percentuale di interesse annuale > double: Tipo predefinito/Primitivo */
        double sommaDepositata = 1000;          /** somma in euro > sempre in double */

        /** double percentualeInteresse = 2.5, sommaDepositata = 1000;     <<<<  si può scrivere anche così, eh ..!!! */
        
        /** E' meglio inizializzare sempre una variabile in java:  anche perchè "te lo fa presente!!!" */


        /** Calcolo interesse dopo un anno:   */

        sommaDepositata=sommaDepositata*(1 + percentualeInteresse/100);   /** Poteva essere anche:   sommaDepositata=sommaDepositata + (sommaDepositata/100) * percentualeInteresse; ... vero??? */
        System.out.println("Dopo un anno avrai: " + sommaDepositata + " euro"); /** Dopo un anno avrai: 1025.0 euro */

        /** Calcolo interesse dopo un altro anno:   */

        sommaDepositata=sommaDepositata*(1 + percentualeInteresse/100);   
        System.out.println("Dopo due anni avrai: " + sommaDepositata + " euro"); /** Dopo due anni avrai: 1050.625 euro */

        /** Calcolo interesse dopo un altro anno ancora:   */

        sommaDepositata=sommaDepositata*(1 + percentualeInteresse/100);   
        System.out.println("Dopo tre anni avrai: " + sommaDepositata + " euro"); /** Dopo tre anni avrai: 1076.890625 euro */

                            
                            /**  Video 11:  I Principali Tipi di Variabili - il byte */
        
    // byte:  tipo di dato "più risparmioso", utilizza 8 bit .......  -128  +127

        byte b1 = 12;       /** Attenzione che Java si accorge se inseriamo valori non ammessi dal range -128  +127!!! */
        byte b2 = -67;      

        //byte b3 = 128;      /** COn 128 mi segna che qualcosa non va!!! */
        //byte b4 = 127+1;      /** Anche così mi segna che qualcosa non va!!! */
        byte b5 = 127+0;      /** Così invece me lo fa passare ...!!! */
        
        byte b6 = 23;
        //byte somma = b1+b6;     /** E Anche qui invece non me lo fa passare -> funzionerebbe:  byte somma = 12+23; !!! */
        

        /** Questo perchè Java è IperMeticoloso e va a controllare tutto spietatamente!!! */
        /** Se non lo facesse si rischierebbe di arrivare ad una violazione ... nella Ram si creerebbero degli errori enormi!!! */


                            /**  Video 12:  La Variabile short, int e long */
    
    // short:  tipo di dato che utilizza 16bit -32768  +32767 (regola: 2^nbit, quindi 2 alla 16°= 65536 bit da dividere però fra positivi e negativi)

        short minValue = -32768;
        short maxValue = 32767;     /** lo zero "0" per convenzione viene rubato dai numeri positivi  */
        //short overTest = 32769;     /** anche per lo short java non ci fa sgarrare e indica l'errore Type mismatch: */

    // int:  tipo di dato che utilizza 32bit -32768  +32767 (regola: 2^nbit, quindi 2 alla 32°= 4.294.967.296 bit che /2 = 2147483648 bit)

        int minValueInt = -2147483648;
        int maxValueInt = 2147483647;   /** pure qui lo zero "0" per convenzione viene rubato dai numeri positivi  */

        /** Maaaa .... e gli "Unsigned"???   in C++ sarebbe così:    */
        //unsigned int x = 4294967296     /** in Java mi da errore: The literal 4294967296 of type int is out of range */
                                        /** Oracle dice che dalla versione Jtk-Se 8 in poi si può utilizzare la classe "Integer" per i valori denza segno */
        
        /** 05:40  Ma come "puristi" java introduciamo invece il tipo long */

    // long:  tipo di dato che utilizza 64bit (quindi 2 alla 64°= 18.446.744.073.709.551.616‬ bit che /2 = 9.223.372.036.854.775.808‬ bit)
        // perciò per i valori negativi si ha: -9223372036854775808‬ e positivi: +9223372036854775807 ... e ne deriva:

        long minValueLong = -9223372036854775808l;
        long maxValueLong = +9223372036854775807l;


                        /**  Video 13:  La Variabile float,  */

// float:  tipo di dato "più risparmioso" per i numeri con virgola, utilizza 32 bit ....... 

        //float x = 3.14;
        //float y = 3.123456789123456789;
        float z = (float) 3.14;         // Operazione di "cast"
        double x = 3.14;
        double y = 3.123456789123456789;
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);

        /** Però però .... però:  la stampa fra i due tipi sarà differente*/
        float zz = (float) 3.12345678;
        double yy = 3.12345678;
        System.out.println(zz + " e " + yy);
        /** Risultato:  3.1234567 e 3.12345678 --- il float, anche se cast non ce la fa e perde una parte dell'informazione ... */

        float zzz = (float) 3.1234567;      // eliminiamo l'ultimo "8" ...
        double yyy = 3.12345678;
        System.out.println(zzz + " e " + yyy);
        /** Risultato:  3.1234567 e 3.12345678 --- Ottimo!! ... */

                        /**  Video 14:  La Variabile double  */

// double: doppio dei byte disponibili rispetto al float!!, utilizza 64 bit (numeri più grandi ma anche molto più precisi) ...

    double limitValueDouble = 3.1234567890123456;
    System.out.println(limitValueDouble);           /** ma anche il double ha i suoi limiti: 3.1234567890123457 -> il "56" è stato arrotondato a 57 */
    double okValueDouble = 3.123456789012345;           
    System.out.println(okValueDouble);             /** mentre togliendo una cifra: 3.123456789012345 ... non c'è arrotondamento!!! */


    double limitValueDouble2 = 312345678901234567d;   /** con gli interi si deve aggiungere la "d" di double dopo il numero */
    System.out.println(limitValueDouble2);          /** Stampa:  3.1234567890123456E17 --> E17 sta per 10^17° cioè spostare la virgola di 17 posti*/
                                                    /** Ma la rappresentabilità del nume"ro si ferma a "56" il resto è inteso dalla forma esponenziale E17 */
    double okValueDouble2 = 3123456789012345d;   /** questo numero verrà "rappresentato" tutto ...*/
    System.out.println(okValueDouble2);          /** Stampa: 3.123456789012345E15  -> E15 sta per 10^15° cioè spostare la virgola di 15 posti: perfetto!!! */

    /** Questa erano i "limiti max" di "rappresentabilità" dei numeri ... maaa ... in valore assoluto????  */
    /** Vediamo la Classe dei numeri float "Float.MAX_VALUE"  positiva e negativa ... */
    
    float maxValueNegativoF = -Float.MAX_VALUE;      //** valore negativo rappresentabile più lontano dallo zero */
    float maxValuePositivoF = Float.MAX_VALUE;       //** valore positivo rappresentabile più lontano dallo zero */    
    System.out.println(maxValueNegativoF + " and " + maxValuePositivoF);  //** Stampa:  -3.4028235E38 and 3.4028235E38  -> E38 = spostare la virgola di 38 posti!!!*/

    /** Vediamo ora la Classe dei numeri double "Double.MAX_VALUE"  positiva e negativa ...Stessa tecnica!!! */

    double maxValueNegativoD = -Double.MAX_VALUE;      //** valore negativo rappresentabile più lontano dallo zero */
    double maxValuePositivoD = Double.MAX_VALUE;       //** valore positivo rappresentabile più lontano dallo zero */    
    System.out.println(maxValueNegativoD + " and " + maxValuePositivoD);  //** Stampa:  -1.7976931348623157E308 and 1.7976931348623157E308  */
                                                                            //** Più cifre ma "sopratutto" l'esponente -> E308 = spostare la virgola di 308 posti!!! */
    /** rappresentabili quindi circa 16 cifre, la parte "pesante" del numero:  le altre sono approssimate */

    /** E "limiti min" di "rappresentabilità" dei numeri????  */
    /** Vediamo la Classe dei numeri double "Double.MIN_VALUE" */

    double minValueNegativoD = Double.MIN_VALUE;      /** valore rappresentabile più "Vicino" allo zero */
    System.out.println(minValueNegativoD);          /** Stampa:  4.9E-324  -> vuol dire 4.9 spostandosi di 324 posti verso sinistra!!!! o,49000 ...  */


                        /**  Video 15:  Le Variabili tipo char, booleans e string  */

// char: delimitato da apici (''), è il modo più "risparmioso" per memorizzare un carattere, 1 byte (2 byte se si usa codifica Unicode)  ...

    char c1 = 'a';  // carattere normale
    char c2 = '#';  // simbolo  
    char c3 = '.';  // punteggiatura
    char c4 = 65;   // carattere numerico (viene memorizzato il codice ASCII 65 -> A)  
    // char c5 = -9;   // Errore -> carattere numerico non valido: solo positivi!!!
    char c6 = 0;    // carattere numerico minimo rappresentabile (Null)
    char c7 = 255;  // carattere numerico massimo rappresentabile

    System.out.println(c1 + " e " + c2 + " e " + c3 + " e " + c4 + " e (c5 no) " + c6 + " e " + c7);    
    /** Risultato: a e # e . e A e (c5 no)   e ÿ    -> perchè c7=ÿ???    non è il 152???*/

    char c8 = 152;
    
    System.out.println("il c8 e': " + c8);      /** Stampa:  il c8 e': ?    -> ed il n° ASCII 63 allora??? */

    char c9 = 63;
    System.out.println("la var c9 (n^ 63) e': " + c9);      /** Stampa:  la var c9 (n^ 63) e': ?  ..... ahhh, ecco!!! */

    int dualita = 100 + 'a';
    System.out.println(dualita);        /** a conferma della "dualita" della variabile char, il Risultato e' 197!! ('a' in ASCII = 97) */

// boolean: scritto da George Bool -> solo 2 elementi: '1' oppure lo '0' ma anche 'vero' o 'falso'  ...

    //boolean veroFalso = false;          /** costante predefinita: sarebbe poco utile immettere 'boolean veroFalso = true' */
                                        /** esempio memorizzando in 'veroFalso' che 5>3 */
    boolean veroFalso = 5>3;            /** e' ovvio ma ... proviamo a stampare: */
    System.out.println(veroFalso);      /** e ovviamente mi stampa: true (in programmazione si usa di più nomi tipo 'controllo' ecc) */

    boolean superatoControllo = 5<3;        /** e' ovvio e proviamo a stampare:   */
    System.out.println(superatoControllo);  /** e ovviamente mi stampa: false */

// String: (non e' come 'string') non è un Tipo Primitivo ma e' già qualcosa di più complesso!  ...
    
    String cognome = "Bergamasco";      /** come nelle stringhe in c++ posso immettere una sequenza di caratteri, ma non solo ... */
    superatoControllo = cognome.endsWith("sco");    /** esempio:  controllo se il cognome finisce o meno con 'sco' */
    System.out.println(superatoControllo);          /** e ovviamente mi stampa: true    -> Oppure:   */
    superatoControllo = cognome.endsWith("sCo");    /** ... cambio una sola consonante da minuscola a maiuscola ... */ 
    System.out.println(superatoControllo);          /** e Ri-ovviamente mi stampa: false */

    /** Nota: è bene utilizzare la 'stringa nulla' per inizializzare 'a zero' la Stringa */
    String nome = "";
    System.out.println(nome);                       /** non stampa nulla ma siamo sicuri che è 'vuota' */

                    /**  Fine Video 15:  Le Variabili tipo char, booleans e string  */




















    










    

                
        
 





          

        
       


    }


}