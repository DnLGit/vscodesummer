#include<iostream>
using namespace std;

/* Scrivere un programma che scrive i primi n numeri primi, con n richiesto e inserito da tastiera. */

/* (Definizione: Un numero naturale, maggiore di 1, si dice primo se è divisibile SOLO per se stesso e per l'unità.)*/
 
int main()
{
    int N;
    int num=2, primo;
    char uscita='s';



    while(uscita!='n'&& uscita!='N')
    {   
        cout<<"\nInserisci quanti numeri vuoi esaminare: ";
        cin>>N;

        for(int x=num; x<=N; x++) // stampa numeri da 2 in poi?? 
        {   

            for(int y=2; y<=x; y++)  // 
            {   
                if(x%y==0 && x!=y) // Condition for not prime (???) (forse divide il numero per 2 e tutti gli altri??)
                {
                    break;          // a cosa serve questo break??
                }
                if(y==x)            // Condition for Prime numbers (??)
                {
                    cout<<x<<" ";
                }
            }            
        }
    cout<<"\nvuoi continuare? (s/n): ";
    cin>>uscita;
    }
    return 0;
}