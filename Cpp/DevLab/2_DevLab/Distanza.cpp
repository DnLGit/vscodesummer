#include<iostream>
#include<cmath>
using namespace std;

/* Scrivere un programma che, dati due punti espressi tramite coordinate (x,y), restituisce la distanza tra i due punti */

int main()
{
    // dichiarazione coordinate dei due punti e della distanza AB (la formula avra' una radice, quindi il Tipo sara' Double)
    double x1, x2, y1, y2;
    double AB=0;
    char uscita='s';

    // cout<< "\nVuoi iniziare? (s/n)";
    // cin>> uscita;

    cout<< "\nPer avere la distanza di due punti, inserisci le loro coordinate: "<<endl;
    
    while(uscita!='n')
    {
        cout<< "\nInserisci x1: ";
        cin>>x1;
        cout<< "\nInserisci x2: ";
        cin>>x2;
        cout<< "\nInserisci y1: ";
        cin>>y1;
        cout<< "\nInserisci y2: ";
        cin>>y2;

        AB=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
        cout<<"La distanza risulta di "<<AB<<" unita'"<<endl;
        cout<< "\nPremi 's' per continuare, 'n' per uscire: ";
        cin>>uscita;
    }
    
    return 0;
}