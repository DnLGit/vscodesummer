#include<iostream>
using namespace std;

/* Scrivere un programma che scrive i primi n numeri primi, con n richiesto e inserito da tastiera. */

/* (Definizione: Un numero naturale, maggiore di 1, si dice primo se è divisibile SOLO per se stesso e per l'unità.)*/
 
bool isPrime(int x)             // prototipo funzione che "ritorna" un numero "se e' Primo".
{
    for(int i=2; i<=x/2; i++)
    {
        if(x%i == 0)
            return false;
    }

    return true;
}

int main()
{
    int N;
    int num=2, primo;
    char uscita='s';



    while(uscita!='n'&& uscita!='N')
    {   
        cout<<"\nInserisci quanti numeri vuoi esaminare: ";
        cin>>N;
        int n = 1;
        for(int count=0; count<N; count++) // conta quanti numeri primi ho trovato fino ad ora
        {   
            while(!isPrime(n)){             // ciclo utilizzando la funzione "isPrime"
                n++;
            }
            cout << n << endl;
            n++;
        }
    cout<<"\nvuoi continuare? (s/n): ";
    cin>>uscita;
    }
    return 0;
}