#include<iostream>
using namespace std;

/* Scrivere un programma che, inseriti i coefficienti a,b,c delle equazioni di due rette, dice se le due rette sono 
    parallele, perpendicolari, incidenti o coincidenti. */

int main()
{
    double a1, b1, c1, a2, b2, c2;
    double m1, m2;
    double q1, q2;
    
    string condizione;


    cout<<"\nInserire i coefficienti a, b e c di due rette di equazione ax+by+c=0 "<<endl;   
  
    cout<<"\nRetta 1, inserire a1: ";
    cin>> a1;
    cout<<"a seguire, b1: ";
    cin>> b1;
    cout<<"e c1: ";
    cin>> c1;
    cout<<"\nRetta 2, inserire a2: ";
    cin>> a2;
    cout<<"a seguire, b2: ";
    cin>> b2;
    cout<<"e c2: ";
    cin>> c2;
    cout<<"\nRispettivamente le due rette avranno queste equazioni:"<<endl;
    cout<<"     Retta 1 --> "<<a1<<"x+"<<b1<<"y+"<<c1<<"=0"<<endl;
    cout<<"     Retta 2 --> "<<a2<<"x+"<<b2<<"y+"<<c2<<"=0"<<endl;

    m1=-a1/b1;
    m2=-a2/b2;
    q1=-c1/b1;
    q2=-c2/b2;
    cout<<"m1 = " <<m1<<"  m2 =: "<<m2<<endl;
    cout<<"q1 = " <<q1<<"  q2 = "<<q2<<endl;


    if(m1==m2 && q1!=q2)
    {
        condizione="Parallele";
    }
    if(m1 == -1/m2)
    {
        condizione="Perpendicolari";
    }
    if(m1!=m2 && q1!=q2)
    {
        condizione="Incidenti";
    }
    if(m1==m2 && q1==q2)
    {
        condizione="Coincidenti";
    }
    
    cout<<"\nRisultano quindi "<<condizione<<" fra loro!"<<endl;
    system("pause");


return 0;
}