#include<iostream>
using namespace std;

/* Scrivere un programma che, dati due punti espressi tramite coordinate (x,y), restituisce l'equazione della retta passante per quei due punti. */
/* (L'equazione della retta e':  y= mx+q) (m= y2-y1/x1-x2) (q= y1-m) */

int main()
{
    double x1, y1, x2, y2;
    double m, q;

    cout<<"\nInserisci le coordinate di due punti nello spazio cartesiano: "<<endl;
    cout<<"\nPrimo punto, inserisci x1: ";
    cin>>x1;
    cout<<"             inserisci y1: ";
    cin>>y1;
    cout<<"\nSecondo punto, inserisci x2: ";
    cin>>x2;
    cout<<"               inserisci y2: ";
    cin>>y2;

    m=(y2-y1)/(x2-x1);
    q= y1 - m * x1;
    cout<<"\nil coefficiente angolare 'm' e': "<<m<<endl;
    cout<<"mentre la quota 'q' e': "<<q<<endl<<endl;

    cout<<"L'equazione della retta passante per i due punti risulta essere: --> ";
    cout<<"y="<<m<<"x+"<<q<<endl;


    system("pause");

    return 0;
}
