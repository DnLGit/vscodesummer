#include<iostream>
using namespace std;

/* Scrivere un programma che comunica tutti i divisori di un numero inserito da tastiera. */

int main()
{
    int num;
    char uscita='s';

    while(uscita!='n' && uscita !='N')
    {
        cout<<"\nInserisci un numero: ";
        cin>>num;
        cout<<"       i divisori di " <<num<< " sono: ";

        for(int x=1; x<=num; x++)
    {   
        if(num%x==0)
        {
            cout<<x<<" ";
        }
    }
        cout<<"\n\nVuoi riprovare? (s/n) --> ";
        cin>>uscita;           
    }


    return 0;
}