#include<iostream>
using namespace std;

/* A scelta dell'utente, calcola il fattoriale di un numero, la somma dei primi N numeri pari (con N inserito da tastiera) o l'mcd tra due numeri */

int main(){

    int scelta, num1, num2, fatt, pari=0, temp, MCD=0;  // probabilmente per la variabile fatt servirà un long-int
    char uscita='n';

    cout<<"\n *** Calcolo Fattoriale di un numero, Somma dei primi N numeri pari, MCD tra due numeri ***"<<endl;

    // Creazione del menu'
    while((uscita!='s')&&(uscita!='S'))                 // condizione per entrare nel menu'
    {    

        cout<< "\nMenu' eventi \nInserisci: ";
        cout<< "\n              1 Fattoriale";
        cout<< "\n              2 SommaPari";
        cout<< "\n              3 MCD";
        cout<< "\n              4 Esci dal Menu'";
        cout<< "\n              Fai la tua scelta: ";
        if( cin >> scelta )                 // "if" per correggere errore di inserimento accidentale di una lettera al posto di un intero
        {
            //cin andato a buon fine
        }else{
            //cin andato in errore
            cin.clear();                     // clears the error flags
                                            // this line discards all the input waiting in the stream
            cin.ignore(256, '\n');          // inserito '256'in modo da essere sicuri di 'far saltare' il cursore fuori dall'errore
        }


        switch(scelta){
            case 1:
            {
                cout<<"\nHai scelto di calcolare il Fattoriale \nInserisci il numero: ";
                cin>> num1;
                if(num1==0)
                {
                    fatt=1;
                } 
                else
                {
                fatt=num1;
                for(int x=1; x<num1; x++)
                {
                    fatt*=(num1-x);
                }
                   
                }
                    
                cout <<"hai inserito il numero "<<num1<<endl;
                cout <<"risulta che il fattoriale di "<<num1<<" e': "<<fatt<<endl;
                system("pause");
                break;
            }
            case 2:
            {
                cout<<"\nHai scelto di calcolare la somma dei primi numeri pari \nInserisci quanti: ";
                cin>> num1;
                pari=0;
                for(int x=1; x<=num1; x++)
                {
                    pari+=x*2;
                }
                cout <<"hai deciso per "<<num1<<" numeri pari"<<endl;
                cout <<"la loro somma risulta "<<pari<<endl;
                system("pause");
                break;
            }
            case 3:
            {
                cout<<"\nHai scelto di calcolare il MCD di due numeri, inseriscili qui: "<<endl;
                cout<<"Inserisci il primo numero: ";
                cin>> num1;
                cout<<"Inserisci il secondo numero: ";
                cin>> num2;

                if(num1>num2) //condizione per scambiare i 2 numeri per avere il minore in "num1".
                {
                    temp=num2;
                    num2=num1;
                    num1=temp; // in questo modo avro' sicuramente e sempre il numero piu' piccolo in "num1" da usare nel For seguente:
                }
                for(int x=num1; x>0; x--)
                {
                    if((num2%x==0)&&(num1%x==0))
                    {
                        MCD=x;
                        x=0;
                    }
                }
                cout <<"hai inserito i numeri "<<num1<<" e "<<num2<<endl;
                cout<<"Il loro MassimoComuneDivisore e': "<<MCD<<endl;
                system("pause");
                break;
            }
            case 4:
            {            
                cout<< "\nHai scelto di uscire, sicuro? (s/n): ";
                cin>> uscita;
                system("pause");
                break;
            }
            default:
            {            
                cout<<"\nOps, inserimento scorretto, riprova"<<endl;
                system("pause");
                break;
            }

        }

    }
return 0;
}
