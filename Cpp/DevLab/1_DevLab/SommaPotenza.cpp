#include <iostream>
using namespace std;

/* A scelta dell'utente, calcola la somma di 10 numeri interi inseriti da tastiera o eleva un numero per un esponente inserito da tastiera */

int main(){

int num, esp, res=0;
int scelta;
char uscita='n';

    while((uscita!='s')&&(uscita!='S')){     // si può scrivere in altro modo, senza ripetere uscita?? (questa e' la regola!!!!)

        /* Creazione del menu' per la scelta*/
        
        cout<< "\nMenu' per la scelta evento \nInserisci: ";
        cout<< "\n         1 per fare la somma dei 10 numeri";
        cout<< "\n         2 per elevare alla potenza il numero";
        cout<< "\n         3 se vuoi uscire";
        cout<< "\n         Fai la tua scelta: ";
        cin>> scelta;

        switch(scelta){
            case 1:
            {
                cout<< "\nHai scelto di fare la somma."<<endl<<endl;
                res=0;
                for(int x=1; x<11; x++){
                    cout<< "Inserisci il " <<x<<"^ numero: ";
                    cin>> num;          // Perche' mi e' uscito come 1° inserimento il -13.195° ???
                                        // ops: avevo messo "num" qui sopra al posto della "x"!!!
                    if (num==0) x=10;    // come funziona questa condizione???
                    else res+=num;      // e' corretto qui che l'else sia all'interno del for?    
                }
                cout<< "La somma e': "<<res<<endl<<endl;
                system("pause");
                break;
            }
            case 2:
            {
                cout<< "\nHai scelto di fare la Potenza."<<endl<<endl;
                cout<< "Inserisci ora il numero: ";
                cin>> num;
                cout<< "Insrisci ora l'esponente: ";
                cin>> esp;
                if(esp==0)
                    res=1;
                else
                    res=num;
                    for(int x=1; x<esp; x++)
                    res*=num;
                cout <<"hai inserito il numero "<<num<<endl;
                cout <<"Quindi "<<num<< " alla " <<esp<<"^ fa: ..."<<res<<endl<<endl;
                system("pause");
                break;

            }
            case 3:
            { 
                cout<< "\nHai scelto di Uscire."<<endl;
                cout<<"Sicuro di uscire? (s/n): ";
                cin>> uscita;
                break;
                
                
            }
            default:
            {
                cout<<"Ops, inserimento scorretto, riprova"<<endl;
                break;
            }
        }
        
    }


    return 0;

} 
/*  Come riconoscere velocemente errori di identazione? Quali sono le regole a terminale?
    In alternativa: qual'e' la modalita' migliore per scrivere codice correttamente identato?
    Dopo lo "switch" le variabili non mi si colorano piu' correttamente: forse perche' il codice non e' completo?
        (ops, avevo scritto "iint" all'interno del for ...)
     */
