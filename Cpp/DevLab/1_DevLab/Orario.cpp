#include<iostream>
using namespace std;

/* Selezionando un giorno della settimana, comunica l'orario scolastico delle lezioni di quel giorno */

int main(){

    int scelta;
    char uscita='n';

    cout<<"\n *** Calendario scolastico Informatica III Serale 2018/2019 ***"<<endl;

    while((uscita!='s')&&(uscita!='S'))
    {
        cout<< "\nMenu' settimanale ";
        cout<< " \nLunedi       -> clicca 1";
        cout<< " \nMartedi      -> clicca 2";
        cout<< " \nMeroledi     -> clicca 3";
        cout<< " \nGiovedi      -> clicca 4";
        cout<< " \nVenerdi      -> clicca 5";
        cout<< " \nSabato       -> clicca 6";
        cout<< " \nDomenica     -> clicca 7";
        cout<< " \nUscita Menu' -> clicca 8\n";
        cout<< "\nFai la tua scelta: ";
        cin>> scelta;

        switch (scelta){
        
            case 1:
            {
                cout<< "\nLunedi:";
                cout<< "\n18:30-19:18 -> Informatica";
                cout<< "\n19:18-20:06 -> TPSIT";
                cout<< "\n20:06-20:54 -> Ita/Sto";
                cout<< "\n20:54-21:04 -> Pausa";
                cout<< "\n21:04-21:52 -> Ita/Sto";
                cout<< "\n21:52-22:40 -> Informatica";
                cout<< "\n22:40-23:28 -> Informatica"<<endl<<endl;
                system("pause");
                break;
            }
            case 2:
            {
                cout<< "\nMartedi:";
                cout<< "\n18:30-19:18 -> Telecomunicazioni";
                cout<< "\n19:18-20:06 -> Telecomunicazioni";
                cout<< "\n20:06-20:54 -> TPSIT";
                cout<< "\n20:54-21:04 -> Pausa";
                cout<< "\n21:04-21:52 -> Inglese";
                cout<< "\n21:52-22:40 -> Ita/Sto";
                cout<< "\n22:40-23:28 -> Informatica"<<endl<<endl;
                system("pause");
                break;
            }
             case 3:
            {
                cout<< "\nMercoledi:";
                cout<< "\n18:30-19:18 -> Informatica";
                cout<< "\n19:18-20:06 -> Matematica";
                cout<< "\n20:06-20:54 -> Matematica";
                cout<< "\n20:54-21:04 -> Pausa";
                cout<< "\n21:04-21:52 -> Informatica";
                cout<< "\n21:52-22:40 -> Informatica";
                cout<< "\n22:40-23:28 -> TPSIT"<<endl<<endl;
                system("pause");
                break;
            }
            case 4:
            {
                cout<< "\nGiovedi:";
                cout<< "\n18:30-19:18 -> Inglese";
                cout<< "\n19:18-20:06 -> Matematica";
                cout<< "\n20:06-20:54 -> Informatica";
                cout<< "\n20:54-21:04 -> Pausa";
                cout<< "\n21:04-21:52 -> Ita/Sto";
                cout<< "\n21:52-22:40 -> -------";
                cout<< "\n22:40-23:28 -> -------"<<endl<<endl;
                system("pause");
                break;
            }
            case 5:
            {
                cout<< "\nVenerdi: (facoltativo)";
                cout<< "\n18:30-19:18 -> Tutoraggio cl.5";
                cout<< "\n19:18-20:06 -> Tutoraggio cl. 3&4";
                cout<< "\n20:06-20:54 -> -------";
                cout<< "\n20:54-21:04 -> -------";
                cout<< "\n21:04-21:52 -> -------";
                cout<< "\n21:52-22:40 -> -------";
                cout<< "\n22:40-23:28 -> -------"<<endl<<endl;
                system("pause");
                break;
            }
            case 6:
            {
                cout<< "\nSabato: (solo il serale)";
                cout<< "\n18:30-19:18 -> -------";
                cout<< "\n19:18-20:06 -> -------";
                cout<< "\n20:06-20:54 -> -------";
                cout<< "\n20:54-21:04 -> -------";
                cout<< "\n21:04-21:52 -> -------";
                cout<< "\n21:52-22:40 -> -------";
                cout<< "\n22:40-23:28 -> -------"<<endl<<endl;
                system("pause");
                break;
            }
            case 7:
            {
                cout<< "\nDomenica: (pausa settimanale)";
                cout<< "\n18:30-19:18 -> -------";
                cout<< "\n19:18-20:06 -> -------";
                cout<< "\n20:06-20:54 -> -------";
                cout<< "\n20:54-21:04 -> -------";
                cout<< "\n21:04-21:52 -> -------";
                cout<< "\n21:52-22:40 -> -------";
                cout<< "\n22:40-23:28 -> -------"<<endl<<endl;
                system("pause");
                break;
            }
            case 8:
            {            
                cout<< "\nHai scelto di uscire, sicuro? (s/n): ";
                cin>> uscita;
                system("pause");
                break;
            }
            default:
            {
                cout<<"\nOps, inserimento scorretto, riprova"<<endl;
                break;
            }
            
        }
    }
return 0;
}


