#include<iostream>
using namespace std;

/* Inserendo il proprio giorno e mese di nascita, comunica il segno zodiacale corrispondente alla data inserita */

int main(){

    int DD = 1, MM = 1;
    
    while(DD!=0 && MM!=0)
    {
        cout<< "\nCalendario Zodiacale, Inserisci la tua data oppure '0' per Uscire: "<<endl;
        cout<< "Giorno di nascita: ";
        cin>> DD;
        cout<< "Mese di nascita: ";
        cin>> MM;
        
        if((DD>0&&DD<32)&&(MM>0&&MM<13))
        {
            
            switch(MM)
            {
                case 01:
                {
                    if(DD>0&&DD<20)
                    {
                        cout<< "\n il tuo segno e' il Capricorno"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'l'Acquario"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 02:
                {
                    if(DD>0&&DD<20)
                    {
                        cout<< "\n il tuo segno e' l'Acquario"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'dei Pesci"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 03:
                {
                    if(DD>0&&DD<21)
                    {
                        cout<< "\n il tuo segno e'dei Pesci"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'dell'Ariete"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 04:
                {
                    if(DD>0&&DD<20)
                    {
                        cout<< "\n il tuo segno e'dell'Ariete"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'del Toro"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 05:
                {
                    if(DD>0&&DD<21)
                    {
                        cout<< "\n il tuo segno e'del Toro"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'dei Gemelli"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 06:
                {
                    if(DD>0&&DD<21)
                    {
                        cout<< "\n il tuo segno e'dei Gemelli"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'del Cancro"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 07:
                {
                    if(DD>0&&DD<23)
                    {
                        cout<< "\n il tuo segno e'del Cancro"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'del Leone"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 8:
                {
                    if(DD>0&&DD<24)
                    {
                        cout<< "\n il tuo segno e'del Leone"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'della Vergine"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 9:
                {
                    if(DD>0&&DD<23)
                    {
                        cout<< "\n il tuo segno e'della Vergine"<<endl;
                    }
                    else if(DD>=23&&DD<31)
                    {
                        cout<<"\n il tuo segno e'della Bilancia"<<endl;
                    }else{
                        cout << "Data non valida" << endl;
                    }
                    system("pause");
                    break;
                }
                case 10:
                {
                    if(DD>0&&DD<23)
                    {
                        cout<< "\n il tuo segno e'della Bilancia"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'dello Scorpione"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 11:
                {
                    if(DD>0&&DD<22)
                    {
                        cout<< "\n il tuo segno e'della Scorpione"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'del Saggittario"<<endl;
                    }
                    system("pause");
                    break;
                }
                case 12:
                {
                    if(DD>0&&DD<22)
                    {
                        cout<< "\n il tuo segno e'del Saggittario"<<endl;
                    }
                    else
                    {
                        cout<<"\n il tuo segno e'del Capricorno"<<endl;
                    }
                    system("pause");
                    break;
                }
                default:
                {
                    cout<<"\nOps, inserimento scorretto, riprova"<<endl;
                    break;
                }                
            }        
        }
        else if(DD!=0 && MM!=0)
        {
            cout << "Data non valida" << endl;
        } 
        else{
            cout << "Chiusura del programma" << endl;
        }
        
    
    


    }
    return 0;
}

/*  cout<<"ok"<<endl;
    system("pause");
    break;  */
