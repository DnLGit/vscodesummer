#include<iostream>
using namespace std;

/* Restituisce il maggiore e il minore di una serie di numeri inseriti da tastiera */

int main()
{
    int N;
    float num, min, max;

    cout<< "\nInserire quanti numeri considerare per il confronto MinMax: ";
    cin>>N;

    cout<<"\nInserire il 1^ numero: ";
    cin>>num;
    min=max=num;

    for(int x=2; x<=N; x++)
    {
        cout<<"Inserire il "<<x<<"^ numero: ";
        cin>>num;        

        if(num<min)
        {
            min=num;
        }
        if(num>max)
        {
            max=num;
        }
    }
    cout<<"\nIl valore maggiore tra i numeri inseriti e': "<<max<<endl;
    cout<<"\nIl valore minore tra i numeri inseriti e': "<<min<<endl<<endl;
    system("pause");
        
    return 0;
}
