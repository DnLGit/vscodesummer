#include<iostream>
#include<cmath>
using namespace std;

/* Risolvere un equazione di secondo grado del tipo ax^2+bx+c=0 */

int main(){

    float a, b, c;
    float x1, x2;
    float Delta;
    
    cout<< "\nInserire a (coefficente 2^ grado) ";
    cin>>a;
    cout<< "\nInserire b (coefficente 1^ grado) ";
    cin>>b;
    cout<< "\nInserire c (termine noto) ";
    cin>>c;

    Delta=b*b-4*a*c;
    x1=(-b+sqrt(Delta))/(2*a);
    x2=(-b-sqrt(Delta))/(2*a);
    
    // controlli cout per correggere errori
    cout<<"Delta e': "<<Delta<<endl;
    cout<<"radice di Delta: "<<sqrt(Delta)<<endl;
    cout<<"il prodotto 2a vale: "<<2*a<<endl;
    cout<<"l'operazione -b-radice di Delta vale: "<<-b-sqrt(Delta)<<endl;
    cout<<"L'operazione intera vale: "<<(-b-sqrt(Delta))/(2*a)<<endl;
    
    

    if(a==0)
    {
        cout<<"\nl'equazione risulta di primo grado e x vale: "<<-c/b<<endl;
    }
    if(b==0&&c==0)
    {
        cout<< "\nL'equazione e' Indeterminata"<<endl;
    }
    if(b==0&&c!=0)
    {
        cout<< "\nL'equazione risulta Impossibile"<<endl;
    }
    if(b!=0&&c==0)
    {
        cout<< "\nL'equazione e' spuria"<<endl;
        cout<< "x1 vale: "<<x1<< " mentre x2 vale: "<<x2<<endl;
    }
    if(Delta<0&&a!=0)
    {
        cout<< "\nL'equazione non ha soluzioni reali"<<endl;
    }
    if(Delta==0&&a!=0)
    {
        cout<< "\nL'equazione ha 2 soluzioni reali e coincidenti"<<endl;
        cout<< "x1 vale: "<<x1<< " e anche x2 vale: "<<x2<<endl;
    }
    if(Delta>0&&a!=0)
    {
        cout<< "\nL'equazione ha 2 soluzioni reali e distinte"<<endl;
        cout<< "x1 vale: "<<x1<< " mentre x2 vale: "<<x2<<endl;
    }


        return 0;
}