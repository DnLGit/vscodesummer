#include <iostream>
using namespace std;

/* Es.3:  Eleva un numero intero per un esponente inserito da tastiera */

int main(){

    int num, esp, res=0;   // perchè inizializzata solo questa variabile? (xche' le altre due non ne hanno bisogno, 
                          // vengono sovrascritte al momento dell'inserimento da parte dell'utente!!)

    cout<<"inserisci un numero e poi l'esponente: "<<endl;
    cout <<"num: ... ";
    cin >> num;
    cout <<"esp: ... ";
    cin >> esp;
    if(esp==0){
      res=1;
    }
    else{
      res=num;                    // else   res=num;   per quale motivo?  (xche' parto dalla base "num elevato num")
        for(int x=1; x<esp; x++){
          res*=num;               // ho bisogno di capire questa corrispondenza: res*=num  è  res=res*num (xche' non e' una proprieta' matematica!!!)

        }
    }
  
    cout <<"hai inserito il numero "<<num<<endl;

    cout <<"Quindi "<<num<< " alla " <<esp<<"° fa: ..."<<res<<endl; 


return 0;

}



