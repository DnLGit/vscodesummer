#include<iostream>
using namespace std;

/* Calcola la media dei voti presi da un numero N di studenti in un compito in classe, con N inserito da tastiera */

int main(){

    int N;
    float voto, somma, media;
    char risposta = 's';

    while(risposta!='n')  // primo ciclo
    
    {
        risposta = 'n';
        while(risposta=='n')    // secondo ciclo per scegliere se continuare o no il calcolo
        {
            cout<<"\nInserisci il numero di studenti della tua classe: ";
            cin>>N;
            cout<<"Confermi il numero di studenti pari a " << N << "? (y/n) --> ";
            cin>>risposta;
        }    


        cout<< "\nOk, inserisci ora i voti di ognuno: "<<endl;

        somma=0;
        for(int x=1; x<=N; x++)
        {
            cout<<"Inserisci voto del "<<x<<"^ studente: ";
            cin>>voto;
            somma=somma+voto;
        }
        cout<<"La somma dei voti e': "<<somma<<endl;
        cout<<"Quindi la media dei voti della classe e': "<<somma/N<<endl;

        cout<<"Vuoi continuare? (y/n) --> ";
        cin>>risposta;
    }

    
        
    return 0;
}
 